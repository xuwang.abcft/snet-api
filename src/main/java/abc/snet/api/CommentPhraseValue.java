package abc.snet.api;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/4
 */
public class CommentPhraseValue implements Serializable {

    private String commentPhrase; // 用来匹配 comment 的关键短语, e.g. 企业名称
    private Object value; // 对应字段的查询条件, e.g. 科大讯飞

    public CommentPhraseValue() {}

    public CommentPhraseValue(String commentPhrase, Object value) {
        this.commentPhrase = commentPhrase;
        this.value = value;
    }

    public String getCommentPhrase() {
        return commentPhrase;
    }

    public void setCommentPhrase(String commentPhrase) {
        this.commentPhrase = commentPhrase;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentPhraseValue commentPhraseValue = (CommentPhraseValue) o;
        return Objects.equals(commentPhrase, commentPhraseValue.commentPhrase) &&
                Objects.equals(value, commentPhraseValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentPhrase, value);
    }

    @Override
    public String toString() {
        return "CommentPhraseValue{" +
                "commentPhrase='" + commentPhrase + '\'' +
                ", value=" + value +
                '}';
    }

}
