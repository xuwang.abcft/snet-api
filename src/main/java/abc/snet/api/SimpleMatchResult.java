package abc.snet.api;

import java.util.List;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/4
 */
public class SimpleMatchResult {

    // 2017年科大讯飞的净利润
    private String sentence; // 原始语句
    private List<String> phrase; // 原始分词
    private String schema; // 默认数据库名（可能为空）
    private String table; // 默认匹配的表名（可能为空）
    private List<CommentPhraseValue> criteria; // 查询条件短语（类） - 值, e.g. 企业 - 科大讯飞
    private List<String> objects; // 查询目标短语, e.g. 股票代码, 总经理

    public SimpleMatchResult() {}

    public SimpleMatchResult(String schema, String table, List<CommentPhraseValue> criteria, List<String> objects) {
        this.schema = schema;
        this.table = table;
        this.criteria = criteria;
        this.objects = objects;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<CommentPhraseValue> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<CommentPhraseValue> criteria) {
        this.criteria = criteria;
    }

    public List<String> getObjects() {
        return objects;
    }

    public void setObjects(List<String> objects) {
        this.objects = objects;
    }

    @Override
    public String toString() {
        return "SimpleMatchResult{" +
                "table='" + table + '\'' +
                ", criteria=" + criteria +
                ", objects=" + objects +
                '}';
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}
