package abc.snet.api.bond.response;

import abc.snet.api.response.FormData;

import java.util.List;

public class BondFormData {

    private String column;

    private Object title;

    private List<FormData> list;

    public BondFormData() {}

    public BondFormData(String titleColumn, Object title) {
        this.column = titleColumn;
        this.title = title;
    }

    public BondFormData(String titleColumn, Object title, List<FormData> formData) {
        this.column = titleColumn;
        this.title = title;
        this.list = formData;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public List<FormData> getList() {
        return list;
    }

    public void setList(List<FormData> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "BondFormData{" +
                "column='" + column + '\'' +
                ", title=" + title +
                ", list=" + list +
                '}';
    }
}
