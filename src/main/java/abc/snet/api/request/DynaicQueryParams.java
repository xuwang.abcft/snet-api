package abc.snet.api.request;

import java.util.List;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class DynaicQueryParams {

    private List<DynamicQueryParam> dynamicQueryParams;
    private String joinType = "and"; // and, or. dynamicQueryParams 查询条件的之间的关系，默认是 and

    public DynaicQueryParams() {}

    public DynaicQueryParams(List<DynamicQueryParam> dynamicQueryParams) {
        this.dynamicQueryParams = dynamicQueryParams;
    }

    public DynaicQueryParams(List<DynamicQueryParam> dynamicQueryParams, String joinType) {
        this.dynamicQueryParams = dynamicQueryParams;
        this.joinType = joinType;
    }

    public List<DynamicQueryParam> getDynamicQueryParams() {
        return dynamicQueryParams;
    }

    public void setDynamicQueryParams(List<DynamicQueryParam> dynamicQueryParams) {
        this.dynamicQueryParams = dynamicQueryParams;
    }

    public String getJoinType() {
        return joinType;
    }

    public void setJoinType(String joinType) {
        this.joinType = joinType;
    }

    @Override
    public String toString() {
        return "DynaicQueryParams{" +
                "dynamicQueryParams=" + dynamicQueryParams +
                ", joinType='" + joinType + '\'' +
                '}';
    }
}
