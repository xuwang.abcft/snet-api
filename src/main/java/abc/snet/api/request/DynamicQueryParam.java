package abc.snet.api.request;

/**
 * 查询条件
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class DynamicQueryParam {

    public static final String eq = "=";
    public static final String iEq = "!=";
    public static final String more = ">";
    public static final String moreEq = ">=";
    public static final String less = "<";
    public static final String lessEq = "<=";
    public static final String like = "like";

    private String title; // 预留字段，暂未使用。比如可以填写 债券名称 这种字段的 COMMENT 及同义词
    private String column; // 查询条件的column名称，可能是 name, xxtable.code1等
    private String logic; // =, > , >=, <, <=, like, !=
    private Object value; // 网页端传过来的都是String类型

    public DynamicQueryParam() {}

    public DynamicQueryParam(String column, Object value) {
        this(column, eq, value);
    }

    public DynamicQueryParam(String column, String logic, Object value) {
        this.column = column;
        this.logic = logic;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getLogic() {
        return logic;
    }

    public void setLogic(String logic) {
        this.logic = logic;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "DynamicQueryParam{" +
                "title='" + title + '\'' +
                ", column='" + column + '\'' +
                ", logic='" + logic + '\'' +
                ", value=" + value +
                '}';
    }
}
