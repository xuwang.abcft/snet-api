package abc.snet.api.response;

import java.util.Objects;

/**
 * 可以用于图表或列表行数据
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class ColumnData {

    protected String column;
    protected Object value;

    public ColumnData() {}

    public ColumnData(String column, String value) {
        this.column = column;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColumnData columnData = (ColumnData) o;
        return Objects.equals(column, columnData.column) &&
                Objects.equals(value, columnData.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, value);
    }

    @Override
    public String toString() {
        return "ColumnData{" +
                "column='" + column + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
