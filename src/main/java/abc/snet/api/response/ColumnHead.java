package abc.snet.api.response;

import java.util.Objects;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class ColumnHead {

    private String title; // text eg 证券代码, 证券简称, 最新债项, 发行人最新评级
    private String column; // code or column name of table

    public ColumnHead() {}

    public ColumnHead(String title, String column) {
        this.title = title;
        this.column = column;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColumnHead that = (ColumnHead) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(column, that.column);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, column);
    }

    @Override
    public String toString() {
        return "ColumnHead{" +
                "title='" + title + '\'' +
                ", column='" + column + '\'' +
                '}';
    }
}
