package abc.snet.api.response;

import java.util.Objects;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/13
 */
public class ColumnHeadData extends ColumnData {

    protected String title;

    public ColumnHeadData() {}

    public ColumnHeadData(String column, Object value, String title) {
        this.column = column;
        this.value = value;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ColumnHeadData that = (ColumnHeadData) o;
        return Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), title);
    }

    @Override
    public String toString() {
        return "ColumnHeadData{" +
                "title='" + title + '\'' +
                ", column='" + column + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
