package abc.snet.api.response;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/13
 */
public class FormData {

    private String title; // text eg 证券代码, 证券简称, 最新债项, 发行人最新评级
    private String color = "black"; // 标题的颜色，是标准颜色值. e.g. red, 默认是 black
    private Map<String, Object> data; // column : value

    public FormData() {}

    public FormData(String title) {
        this.title = title;
    }

    public FormData(String title, String color) {
        this.title = title;
        this.color = color;
    }

    public FormData(String title, Map<String, Object> data) {
        this.title = title;
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Object> getData() {
        if (data == null) data = new LinkedHashMap<>();
        return data;
    }

    public void setData(LinkedHashMap<String, Object> data) {
        this.data = data;
    }

    public FormData putData(String column, Object value) {
        getData().put(column, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FormData formData = (FormData) o;
        return Objects.equals(title, formData.title) &&
                Objects.equals(data, formData.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, data);
    }

    @Override
    public String toString() {
        return "FormData{" +
                "title='" + title + '\'' +
                ", data=" + data +
                '}';
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
