package abc.snet.api.response;

import java.util.ArrayList;
import java.util.List;

/**
 * 地图图表数据
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class MapChartData {

    private String title; // 图表的标题
    private List<MapColumnData> rowData; // 图表的数据类型
    private String chartType = "basicMap"; // 图表类型：世界地图，区域地图
    private List<ColumnHead> columnHeads;

    public MapChartData() {}

    public MapChartData(String title, List<MapColumnData> rowData) {
        this.title = title;
        this.rowData = rowData;
    }

    public MapChartData(String title, List<MapColumnData> rowData, String chartType) {
        this(title, rowData);
        this.chartType = chartType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<MapColumnData> getRowData() {
        if (rowData == null) {
            rowData = new ArrayList<>();
        }
        return rowData;
    }

    public void setRowData(List<MapColumnData> rowData) {
        this.rowData = rowData;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public List<ColumnHead> getColumnHeads() {
        if (columnHeads == null) {
            columnHeads = new ArrayList<>();
        }
        return columnHeads;
    }

    public void setColumnHeads(List<ColumnHead> columnHeads) {
        this.columnHeads = columnHeads;
    }
}
