package abc.snet.api.response;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class MapColumnData extends NamedColumnData {

    private String remark; // 描述，可以是业务值的含义描述，eg 国内生产总值依据汇率结算（美元）
    private String code; // 地区编号

    public MapColumnData() {}

    public MapColumnData(String column, String value, String name) {
        super(column, value, name);
    }

    public MapColumnData(String column, String value, String name, String code) {
        super(column, value, name);
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
