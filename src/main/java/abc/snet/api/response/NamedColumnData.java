package abc.snet.api.response;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class NamedColumnData extends ColumnData {

    protected String name;

    public NamedColumnData() {}

    public NamedColumnData(String column, String value, String name) {
        this.column = column;
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NamedColumnData{" +
                "name='" + name + '\'' +
                ", column='" + column + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
