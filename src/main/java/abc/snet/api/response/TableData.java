package abc.snet.api.response;

import java.util.List;
import java.util.Map;

/**
 * TODO 注解约束，或引入 Jackson 限制 get
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class TableData {

    /**
     * @return all getHeaders. eg 证券代码, 证券简称, 最新债项, 发行人最新评级
     */
    private List<ColumnHead> headerOptions;

    /**
     * @return all getHeaders. eg 证券代码, 证券简称, 最新债项, 发行人最新评级
     */
    private List<ColumnHead> headers;

    private List<Map<String, Object>> data;

    public List<ColumnHead> getHeaderOptions() {
        return headerOptions;
    }

    public void setHeaderOptions(List<ColumnHead> headerOptions) {
        this.headerOptions = headerOptions;
    }

    public List<ColumnHead> getHeaders() {
        return headers;
    }

    public void setHeaders(List<ColumnHead> headers) {
        this.headers = headers;
    }

    public List<Map<String, Object>> getData() {
        return data;
    }

    public void setData(List<Map<String, Object>> data) {
        this.data = data;
    }
}
