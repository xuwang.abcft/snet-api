package abc.snet.api.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 树形结构
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/13
 */
public class TreeNode {

    private Object id;
    private String code;
    private String name;
    private Object value;
    private int level;
    private Long parentId;
    private boolean directory; // 是否目录，否的话一定没有下级
    private List<TreeNode> children;

    /**
     * 映射表名
     */
    private String mapTable;

    /**
     * 映射字段
     */
    private String mapColumn;

    public TreeNode() {}

    public TreeNode(Object id, String name) {
        this.id = id;
        this.name = name;
    }

    public TreeNode(Object id, String name, Long parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    public Object getId() {
        return id;
    }
    public void setId(Object id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }

    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }

    public Long getParentId() {
        return parentId;
    }
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public List<TreeNode> getChildren() {
        if (children == null) children = new ArrayList<>();
        return children;
    }
    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public boolean getDirectory() {
        return directory;
    }
    public void setDirectory(boolean directory) {
        this.directory = directory;
    }

    public String getMapTable() {
        return mapTable;
    }

    public void setMapTable(String mapTable) {
        this.mapTable = mapTable;
    }

    public String getMapColumn() {
        return mapColumn;
    }

    public void setMapColumn(String mapColumn) {
        this.mapColumn = mapColumn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeNode treeNode = (TreeNode) o;
        return level == treeNode.level &&
                directory == treeNode.directory &&
                Objects.equals(id, treeNode.id) &&
                Objects.equals(code, treeNode.code) &&
                Objects.equals(name, treeNode.name) &&
                Objects.equals(value, treeNode.value) &&
                Objects.equals(parentId, treeNode.parentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, value, level, parentId, directory);
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", level=" + level +
                ", parentId=" + parentId +
                ", directory=" + directory +
                ", children=" + children +
                ", mapTable='" + mapTable + '\'' +
                ", mapColumn='" + mapColumn + '\'' +
                '}';
    }
}
