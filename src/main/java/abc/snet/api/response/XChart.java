package abc.snet.api.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @see XColumnChart
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/10
 */
public class XChart {

    private String title; // 图表的标题
    private String subTitle;
    private String subSecondTitle;
    private String subThirdTitle;
    private List<ColumnHead> columnHeads;
    private List<Map<String, Object>> columnData; // 图表的数据类型
    private String chartType = "spline"; // 图表类型：折线图，柱状图
    private String xAxis; // X轴 字段
    private String xTitle = ""; // X 轴的标题
    private String yTitle = ""; // Y 轴的标题
    private String yUnit = ""; // Y 轴的单位
    private String y1Title = ""; // Y 轴（右）的标题
    private String y1Unit = ""; // Y 轴（右）的单位

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ColumnHead> getColumnHeads() {
        if (columnHeads == null) {
            columnHeads = new ArrayList<>();
        }
        return columnHeads;
    }

    public void setColumnHeads(List<ColumnHead> columnHeads) {
        this.columnHeads = columnHeads;
    }

    public List<Map<String, Object>> getColumnData() {
        if (columnData == null) {
            columnData = new ArrayList<>();
        }
        return columnData;
    }

    public void setColumnData(List<Map<String, Object>> columnData) {
        this.columnData = columnData;
    }

    public String getChartType() {
        return chartType;
    }
    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public String getxAxis() {
        return xAxis;
    }
    public void setxAxis(String xAxis) {
        this.xAxis = xAxis;
    }

    public String getxTitle() {
        return xTitle;
    }
    public void setxTitle(String xTitle) {
        this.xTitle = xTitle;
    }

    public String getyTitle() {
        return yTitle;
    }
    public void setyTitle(String yTitle) {
        this.yTitle = yTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }
    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getyUnit() {
        return yUnit;
    }
    public void setyUnit(String yUnit) {
        this.yUnit = yUnit;
    }

    public String getY1Title() {
        return y1Title;
    }
    public void setY1Title(String y1Title) {
        this.y1Title = y1Title;
    }

    public String getY1Unit() {
        return y1Unit;
    }
    public void setY1Unit(String y1Unit) {
        this.y1Unit = y1Unit;
    }

    public String getSubSecondTitle() {
        return subSecondTitle;
    }

    public void setSubSecondTitle(String subSecondTitle) {
        this.subSecondTitle = subSecondTitle;
    }

    public String getSubThirdTitle() {
        return subThirdTitle;
    }

    public void setSubThirdTitle(String subThirdTitle) {
        this.subThirdTitle = subThirdTitle;
    }
}
