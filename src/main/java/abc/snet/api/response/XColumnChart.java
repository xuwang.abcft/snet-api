package abc.snet.api.response;

import java.util.ArrayList;
import java.util.List;

/**
 * 以X轴为参考，Y轴是值
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class XColumnChart {

    private String title; // 图表的标题
    private List<ColumnHead> columnHeads;
    private List<List<ColumnData>> columnData; // 图表的数据类型
    private String chartType = "spline"; // 图表类型：折线图，柱状图
    private String xAxis; // X轴 字段
    private String xTitle = ""; // X轴的标题
    private String yTitle = ""; // Y轴的标题

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ColumnHead> getColumnHeads() {
        if (columnHeads == null) {
            columnHeads = new ArrayList<>();
        }
        return columnHeads;
    }

    public void setColumnHeads(List<ColumnHead> columnHeads) {
        this.columnHeads = columnHeads;
    }

    public List<List<ColumnData>> getColumnData() {
        if (columnData == null) {
            columnData = new ArrayList<>();
        }
        return columnData;
    }

    public void setColumnData(List<List<ColumnData>> columnData) {
        this.columnData = columnData;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public String getxAxis() {
        return xAxis;
    }

    public void setxAxis(String xAxis) {
        this.xAxis = xAxis;
    }

    public String getxTitle() {
        return xTitle;
    }

    public void setxTitle(String xTitle) {
        this.xTitle = xTitle;
    }

    public String getyTitle() {
        return yTitle;
    }

    public void setyTitle(String yTitle) {
        this.yTitle = yTitle;
    }

}
