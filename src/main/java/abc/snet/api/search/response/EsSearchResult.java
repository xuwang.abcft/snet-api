package abc.snet.api.search.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/15
 */
public class EsSearchResult {

	/**查询耗时**/
	private long timeCost = 0;
	
	/**总共命中的条目**/
	private long totalMatched = 0;
	
	private List<EsSearchResultData> resultData;

	private boolean success = true; // 是否未发生异常
    private String msg; // 备注说明

	public EsSearchResult() {}

    public EsSearchResult(boolean success, String msg) {
	    this.success = success;
	    this.msg = msg;
    }
	
	public EsSearchResult(long timeCost, long totalMatched, List<EsSearchResultData> resultData) {
		this.timeCost = timeCost;
		this.totalMatched = totalMatched;
		this.resultData = resultData;
	}

	public long getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(long timeCost) {
		this.timeCost = timeCost;
	}

	public long getTotalMatched() {
		return totalMatched;
	}

	public void setTotalMatched(long totalMatched) {
		this.totalMatched = totalMatched;
	}

	public List<EsSearchResultData> getResultData() {
		if (resultData == null) resultData = new ArrayList<>();
		return resultData;
	}

	public void setResultData(List<EsSearchResultData> resultData) {
		this.resultData = resultData;
	}

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
