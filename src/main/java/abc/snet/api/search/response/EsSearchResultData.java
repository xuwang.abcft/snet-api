package abc.snet.api.search.response;

import java.util.Map;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/15
 */
public class EsSearchResultData {

	private float score;
	private Map<String, Object> data;

	public EsSearchResultData() {}
	
	public EsSearchResultData(float score, Map<String, Object> data) {
		this.score = score;
		this.data = data;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "EsSearchResult [score=" + score + ", data=" + data + "]";
	}

}
