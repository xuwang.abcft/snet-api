package abc.snet.api.snet.response;

import java.util.Objects;

/**
 * 跟蛛网返回数据结构保持一致
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/6
 */
public class SnetColumnName {

    private String code;
    private String name;

    public SnetColumnName() {}

    public SnetColumnName(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SnetColumnName that = (SnetColumnName) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(code, name);
    }

    @Override
    public String toString() {
        return "SnetColumnName{" +
                "column='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
