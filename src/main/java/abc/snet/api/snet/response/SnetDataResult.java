package abc.snet.api.snet.response;

import java.util.List;

/**
 * 跟蛛网返回数据结构保持一致
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/6
 */
public class SnetDataResult {

    private List<Object []> data;
    private List<SnetColumnName> column;
    private String title; // 可以为空，只是跟蛛网保持一致. e.g. 债券数据
    private String type; // e.g. invest_event

    public SnetDataResult() {}

    public SnetDataResult(List<Object[]> data, List<SnetColumnName> column) {
        this.data = data;
        this.column = column;
    }

    public SnetDataResult(List<Object[]> data, List<SnetColumnName> column, String title, String type) {
        this(data, column);
        this.title = title;
        this.type = type;
    }

    public List<Object[]> getData() {
        return data;
    }

    public void setData(List<Object[]> data) {
        this.data = data;
    }

    public List<SnetColumnName> getColumn() {
        return column;
    }

    public void setColumn(List<SnetColumnName> column) {
        this.column = column;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
