package abc.snet.api.snet.response;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/7
 */
public class SnetMatchResContent {

    private String sentence; // 原始输入
    private SnetMatchResContentMatchList matchList;
    private String logicalCaculation;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public SnetMatchResContentMatchList getMatchList() {
        return matchList;
    }

    public void setMatchList(SnetMatchResContentMatchList matchList) {
        this.matchList = matchList;
    }

    public String getLogicalCaculation() {
        return logicalCaculation;
    }

    public void setLogicalCaculation(String logicalCaculation) {
        this.logicalCaculation = logicalCaculation;
    }

    @Override
    public String toString() {
        return "SnetMatchResContent{" +
                "sentence='" + sentence + '\'' +
                ", matchList=" + matchList +
                ", logicalCaculation='" + logicalCaculation + '\'' +
                '}';
    }
}
