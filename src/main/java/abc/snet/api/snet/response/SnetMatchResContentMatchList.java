package abc.snet.api.snet.response;

import java.util.List;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/7
 */
public class SnetMatchResContentMatchList {

    private List<SnetWord> wordsList;
    private List<SnetWord> wordsList_src;
    private List<SnetTemplate> templateList; // 匹配到的模板

    public List<SnetWord> getWordsList() {
        return wordsList;
    }

    public void setWordsList(List<SnetWord> wordsList) {
        this.wordsList = wordsList;
    }

    public List<SnetWord> getWordsList_src() {
        return wordsList_src;
    }

    public void setWordsList_src(List<SnetWord> wordsList_src) {
        this.wordsList_src = wordsList_src;
    }

    public List<SnetTemplate> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<SnetTemplate> templateList) {
        this.templateList = templateList;
    }

    @Override
    public String toString() {
        return "SnetMatchResContentMatchList{" +
                "wordsList=" + wordsList +
                ", wordsList_src=" + wordsList_src +
                ", templateList=" + templateList +
                '}';
    }
}
