package abc.snet.api.snet.response;

import java.util.List;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/7
 */
public class SnetMatchResult {

    private String resResult;
    private List<SnetMatchResContent> resContent;
    private int resCode = 0;

    public String getResResult() {
        return resResult;
    }

    public void setResResult(String resResult) {
        this.resResult = resResult;
    }

    public List<SnetMatchResContent> getResContent() {
        return resContent;
    }

    public void setResContent(List<SnetMatchResContent> resContent) {
        this.resContent = resContent;
    }

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    @Override
    public String toString() {
        return "SnetMatchResult{" +
                "resResult='" + resResult + '\'' +
                ", resContent=" + resContent +
                ", resCode=" + resCode +
                '}';
    }
}
