package abc.snet.api.snet.response;

import java.util.ArrayList;
import java.util.List;

/**
 * 蛛网数据查询结果的数据结构
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/6
 */
public class SnetQueryResult {

    private List<SnetDataResult> result = new ArrayList<>();
    private int score; // e.g. 5
    private String source; // e.g. structural_chuangtou
    private List<String> queryList; // e.g. ["近5年启明创投投资的公司"]
    private String error; // 错误信息

    public SnetQueryResult() {}

    public SnetQueryResult(List<SnetDataResult> snetDataResults) {
        this.result = snetDataResults;
    }

    public SnetQueryResult(List<SnetDataResult> snetDataResults, int score, String source, List<String> queryList) {
        this.result = snetDataResults;
        this.score = score;
        this.source = source;
        this.queryList = queryList;
    }

    public List<SnetDataResult> getResult() {
        return result;
    }

    public void setResult(List<SnetDataResult> result) {
        this.result = result;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<String> getQueryList() {
        return queryList;
    }

    public void setQueryList(List<String> queryList) {
        this.queryList = queryList;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
