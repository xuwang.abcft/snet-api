package abc.snet.api.snet.response;

/**
 * snet 语义模板
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/7
 */
public class SnetTemplate {

    private Long index; // 模板id
    private String templateDisplay; // eg [时间范围][企业名称]
    private String scene; // 场景，eg finance

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public String getTemplateDisplay() {
        return templateDisplay;
    }

    public void setTemplateDisplay(String templateDisplay) {
        this.templateDisplay = templateDisplay;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    @Override
    public String toString() {
        return "SnetTemplate{" +
                "index=" + index +
                ", templateDisplay='" + templateDisplay + '\'' +
                ", scene='" + scene + '\'' +
                '}';
    }
}
