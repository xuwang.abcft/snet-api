package abc.snet.api.snet.response;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/7
 */
public class SnetWord {

    private String flag; // eg NUMBER_RANG
    private int isCombine; // eg 1
    private String subtemplateName; // 数值范围
    private String words; // eg 16-01, 2013-2017

    public SnetWord() {}

    public SnetWord(String flag, int isCombine, String words) {
        this.flag = flag;
        this.isCombine = isCombine;
        this.words = words;
    }

    public SnetWord(String flag, int isCombine, String subtemplateName, String words) {
        this(flag, isCombine, words);
        this.subtemplateName = subtemplateName;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getIsCombine() {
        return isCombine;
    }

    public void setIsCombine(int isCombine) {
        this.isCombine = isCombine;
    }

    public String getSubtemplateName() {
        return subtemplateName;
    }

    public void setSubtemplateName(String subtemplateName) {
        this.subtemplateName = subtemplateName;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    @Override
    public String toString() {
        return "SnetWord{" +
                "flag='" + flag + '\'' +
                ", isCombine=" + isCombine +
                ", subtemplateName='" + subtemplateName + '\'' +
                ", words='" + words + '\'' +
                '}';
    }
}
