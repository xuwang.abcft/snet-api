package abc.snet.api.request;

import java.util.Arrays;

/**
 * @author hilbert.wang@hotmail.com<br>
 * Created on 2018/7/9
 */
public class QueryParamsTests {

    public static void main(String[] args) {
        DynaicQueryParams params = new DynaicQueryParams(Arrays.asList(new DynamicQueryParam("type", "国债"), new DynamicQueryParam("name", DynamicQueryParam.like, "16圣牧")));
        System.out.println(params);
    }

}
